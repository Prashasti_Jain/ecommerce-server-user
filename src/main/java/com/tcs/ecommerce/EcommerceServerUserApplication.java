package com.tcs.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages = { "com.tcs.ecommerce" })
@EnableTransactionManagement
@EnableAutoConfiguration
public class EcommerceServerUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceServerUserApplication.class, args);
	}

}
