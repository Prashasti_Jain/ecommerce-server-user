package com.tcs.ecommerce.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.ecommerce.entity.User;
import com.tcs.ecommerce.model.AddressModelRequest;
import com.tcs.ecommerce.model.AddressResponse;
import com.tcs.ecommerce.entity.Address;
import com.tcs.ecommerce.model.UserRegistrationRequest;
import com.tcs.ecommerce.model.UserRegistrationResponse;
import com.tcs.ecommerce.service.UserDetailsService;
import com.tcs.ecommerce.service.UserDetailsServiceImpl;

@CrossOrigin(origins = "http://13.233.184.83")
@RestController
@Transactional
public class UserController {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private UserDetailsServiceImpl service;

	@RequestMapping(method = RequestMethod.POST, value = "/registeruser", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<UserRegistrationResponse> registerUser(

			@RequestBody UserRegistrationRequest userRegistrationRequest) throws Exception {
		return new ResponseEntity<>(userDetailsService.registerUser(userRegistrationRequest), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sendOTP/{username}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<?> sendOtp(@PathVariable String username) throws Exception {
		ResponseEntity<?> response = null;
		User userObj = userDetailsService.sendOtp(username);
		response = new ResponseEntity<>(userObj, HttpStatus.OK);
		return response;
	}

	@PostMapping("/forgotPasswordWithOTP")

	public ResponseEntity<?> forgotPassword(@RequestBody User user) throws Exception {
		ResponseEntity<?> response = null;
		User userObj = userDetailsService.forgotPasswordByOTP(user.getPassword(), user.getOtp(), user.getEmail());
		response = new ResponseEntity<>(userObj, HttpStatus.ACCEPTED);
		return response;

	}

	@GetMapping("/adminApproval/{kycStatus}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> findByKycStatus(@PathVariable("kycStatus") String kycStatus) throws Exception {
		ResponseEntity<?> response = null;
		List<User> userlist = userDetailsService.findInactiveUser(kycStatus);
		response = new ResponseEntity<>(userlist, HttpStatus.OK);
		return response;
	}

	@PutMapping("/adminApproval")
	@PreAuthorize("hasRole('ADMIN')")
	public User adminApproval(@RequestBody User user) throws Exception {
		return userDetailsService.approveUser(user);
	}

	@PutMapping("/denyUser")
	@PreAuthorize("hasRole('ADMIN')")
	public User denyUser(@RequestBody User user) throws Exception {
		return userDetailsService.denyUser(user);
	}

	@PostMapping("/forgotpasswordwithSecurityQuestion")
	public boolean forgotPasswordwithsecurityQue(@RequestBody User user) throws Exception {
		return userDetailsService.forgotpasswordByQuestion(user);
	}

	@GetMapping("/findUserByEmail")
	public User fetchUserByEmail(@AuthenticationPrincipal String username) {
		return userDetailsService.fetchUserByEmail(username);
	}

	@GetMapping("/findUserByUsername")
	public User fetchUserByUsername(@AuthenticationPrincipal String username) {
		return userDetailsService.fetchUserByUsername(username);
	}

	@GetMapping("/getAddress")
	public Address getAddress(@AuthenticationPrincipal String username) {
		return userDetailsService.getAddress(username);
	}

	@GetMapping("/allMyAddress")
	public List<Address> getAllMyAddress(@AuthenticationPrincipal String username) {
		return userDetailsService.getAllMyAddress(username);
	}

	@PostMapping("/getMyAddresses")
	public List<Address> getMyAddresses(@AuthenticationPrincipal String username, @RequestBody int[] addrList) {
		return userDetailsService.getMyAddresses(username, addrList);
	}

	@DeleteMapping("/deleteMyAddressById/{addId}")
	public String deleteMyAddressById(@PathVariable Integer addId, @AuthenticationPrincipal String username) {
		return userDetailsService.deleteMyAddressById(addId, username);
	}

	@PostMapping("/saveNewAddress")
	public Address saveAddress(@AuthenticationPrincipal String username, @RequestBody AddressModelRequest address) {
		return userDetailsService.saveAddress(address, username);
	}

	@PostMapping("/updateMySingleAddress")
	public Address updateAddress(@AuthenticationPrincipal String username, @RequestBody Address address) {
		return userDetailsService.editMySingleAddress(address, username);
	}

	@GetMapping("/findUserByName")
	public User fetchUserByName(@RequestParam String firstName) {
		return userDetailsService.fetchUserByFirstName(firstName);
	}

	@GetMapping("/findUserByContact")
	public User fetchUserByContact(@RequestParam String primaryContact) {
		return userDetailsService.fetchUserByConatctNumber(primaryContact);
	}

	@PostMapping("/updateUserdetails")
	public User updateUserDetails(@RequestBody UserRegistrationRequest user, @AuthenticationPrincipal String username)
			throws Exception {

		return userDetailsService.updateUserDetails(user, username);

	}

	@RequestMapping(method = RequestMethod.POST, value = "/addressDetail", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<AddressResponse> addAddress(@RequestBody AddressModelRequest address,
			@AuthenticationPrincipal String username) throws Exception {

		return new ResponseEntity<>(userDetailsService.addAddress(address, username), HttpStatus.CREATED);
	}

	@GetMapping("/userinfo")
	public ResponseEntity<?> getSelfUser(@AuthenticationPrincipal String username) {
		ResponseEntity<?> response = null;
		response = new ResponseEntity<>(service.loadUserByUsername(username), HttpStatus.OK);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/allUsers")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<User>> findAllProducts() throws Exception {
		return new ResponseEntity<>(userDetailsService.getUsers(), HttpStatus.OK);
	}

}
