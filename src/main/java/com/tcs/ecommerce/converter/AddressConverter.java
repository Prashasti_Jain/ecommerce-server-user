package com.tcs.ecommerce.converter;

import com.tcs.ecommerce.entity.Address;
import com.tcs.ecommerce.entity.User;
import com.tcs.ecommerce.model.AddressModelRequest;
import java.util.Date;
public final class AddressConverter {

	private AddressConverter() {
		
	}
	
	public static Address convertAddressModelToEntity(AddressModelRequest add,User user) {
		
		Address entityObj = new Address();
		
		entityObj.setAddress(add.getAddress());
		entityObj.setLastName(add.getLastName());
		entityObj.setFirstName(add.getFirstName());
		entityObj.setCity(add.getCity());
		entityObj.setState(add.getState());
		entityObj.setContactNumber(add.getContactNumber());
		entityObj.setCreatedBy(user.getEmail());
		entityObj.setUser(user);
		entityObj.setCreatedOn(new Date());
		entityObj.setModifiedBy(user.getEmail());
		entityObj.setModifiedOn(new Date());
		entityObj.setZipCode(add.getZipCode());
		entityObj.setCountry("India");
		entityObj.setZipCode(add.getZipCode());
		entityObj.setCurrent(add.getCurrent());
		
		return entityObj;
		
	}
}
