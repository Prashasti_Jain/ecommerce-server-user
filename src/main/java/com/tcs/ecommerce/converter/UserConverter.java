package com.tcs.ecommerce.converter;

import java.util.Date;

import com.tcs.ecommerce.entity.Role;
import com.tcs.ecommerce.entity.User;
import com.tcs.ecommerce.model.UserRegistrationRequest;

public final class UserConverter {

	private UserConverter() {

	}

	public static User convertRegisterUserModeltoEntity(UserRegistrationRequest user, Role role) {

		User entityObj = new User();

		entityObj.setEmail(user.getEmail());
		entityObj.setPassword(user.getPassword());
		entityObj.setFirstName(user.getFirstName());
		entityObj.setMiddleName(user.getMiddleName());
		entityObj.setLastName(user.getLastName());
		entityObj.setPrimaryContact(user.getPrimaryContact());
		entityObj.setAlternateContact(user.getAlternateContact());
		entityObj.setGender(user.getGender());
		entityObj.setRole(role);
		entityObj.setSecurityAnswer(user.getSecurityAnswer());
		entityObj.setSecurityQuestion(user.getSecurityQuestion());
		entityObj.setModifiedBy("Sytem");
		entityObj.setModifiedOn(new Date());
		entityObj.setCreatedBy("System");
		entityObj.setCreatedOn(new Date());
		entityObj.setKycStatus("Pending");

		return entityObj;

	}

	public static User convertUserToResponse(User user) {
		User tempUser = new User();
		tempUser.setAlternateContact(user.getAlternateContact());
		tempUser.setPrimaryContact(user.getPrimaryContact());
		tempUser.setEmail(user.getEmail());
		tempUser.setCreatedBy(user.getCreatedBy());
		tempUser.setCreatedOn(user.getCreatedOn());
		tempUser.setFirstName(user.getFirstName());
		tempUser.setMiddleName(user.getMiddleName());
		tempUser.setLastName(user.getLastName());
		tempUser.setModifiedBy(user.getModifiedBy());
		tempUser.setModifiedOn(user.getModifiedOn());
		tempUser.setGender(user.getGender());
		tempUser.setKycStatus(user.getKycStatus());
		tempUser.setRole(user.getRole());
		tempUser.setUserId(user.getUserId());
		return tempUser;
	}

}
