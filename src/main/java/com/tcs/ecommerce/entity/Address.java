package com.tcs.ecommerce.entity;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "seqAddr", initialValue = 26, allocationSize = 1)
@Table(name = "\"Address\"")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqAddr")
	private Integer addressId;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String state;
	private String country;
	private int zipCode;
	private String contactNumber;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	private Date modifiedOn;
	private String deleteStatus;
	private boolean current;
	@ManyToOne
	@JoinColumn(name = "USER_ID", foreignKey = @ForeignKey(name = "ADRRESS_TO_ADRRESS_USER_CNSTRNT_USER_ID_FK"))
	private User user;

	public boolean getCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

}
