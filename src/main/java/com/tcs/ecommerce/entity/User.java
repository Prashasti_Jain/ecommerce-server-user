package com.tcs.ecommerce.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "seqUser", initialValue = 4, allocationSize = 1)
@Table(name = "\"User_details\"")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqUser")
	private Integer userId;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	private String primaryContact;

	@Column(nullable = false)
	private String firstName;
	private String middleName;
	@Column(nullable = false)
	private String lastName;

	private String alternateContact;

	private String gender;
	private String securityQuestion;
	private String securityAnswer;
	private String kycStatus;
	private String createdBy;
	private Date createdOn;
	private String modifiedBy;
	@JsonProperty(access = Access.WRITE_ONLY)
	private char[] otp;

	private Date modifiedOn;

	@ManyToOne
	@JoinColumn(name = "ROLE_ID", foreignKey = @ForeignKey(name = "USER_TO_USER_ROLE_CNSTRNT_ROLE_ID_FK"))
	private Role role;

	
}