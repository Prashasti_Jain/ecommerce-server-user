package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class AddressModelRequest {
	private Integer addressId;
	private Integer userId;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String state;
	private int zipCode;
	private String contactNumber;
	private String country;
	private boolean current;

	public boolean getCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	
	
	
}
