package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class AddressResponse {

	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String contactNumber;
	private boolean current;
	private int zipCode;
	private int addressId;

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

}
