package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class ImageResponse {

	private Integer imageId;
	private String imageURL;

}
