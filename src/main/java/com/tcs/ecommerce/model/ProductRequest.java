package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class ProductRequest {

	private String productName;
	private String productDesc;
	private Double productPrice;
	private String productStatus;
	private Integer categoryId;
	private String imageIds;

}
