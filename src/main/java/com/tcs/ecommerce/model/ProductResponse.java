package com.tcs.ecommerce.model;

import java.util.List;

import lombok.Data;

@Data
public class ProductResponse {

	private Integer productId;
	private String productName;
	private String productDesc;
	private Double productPrice;
	private String productStatus;
	private Integer categoryId;
	private String categoryName;
	private List<ImageResponse> image;
	private String sellerName;
}
