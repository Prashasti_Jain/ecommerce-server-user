package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class UserRegistrationRequest {

	private String email;
	private String password;
	private String firstName;
	private String middleName;
	private String lastName;
	private String primaryContact;
	private String alternateContact;
	private String gender;
	private String securityQuestion;
	private String securityAnswer;
	private Integer roleId;

}
