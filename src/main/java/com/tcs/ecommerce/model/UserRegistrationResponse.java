package com.tcs.ecommerce.model;

import lombok.Data;

@Data
public class UserRegistrationResponse {

	private String email;
	private String firstName;
	private String lastName;
	private String primaryContact;

}
