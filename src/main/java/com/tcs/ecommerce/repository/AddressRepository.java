package com.tcs.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tcs.ecommerce.entity.Address;
import com.tcs.ecommerce.entity.User;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

	Address findByUser(User findByEmail);

	Address findOneByUser(User findByEmail);

	List<Address> findAllByUser(User findByEmail);

	List<Address> findAllByAddressIdIn(int[] addrList);

	Address findByAddressId(int i);

	void deleteByAddressId(Integer addressId);

}
