package com.tcs.ecommerce.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.tcs.ecommerce.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	@Query("select u from User u where u.kycStatus = :kycStatus")
	public User findByKYCStatus(String kycStatus);

	public List<User> findAll();

	public User findAllByEmail(String email);

	public User findAllByEmailAndPassword(String email, String password);

	@Query("select u from User u where u.email = :email")
	public User findByEmail(String email);

	public User findByFirstName(String firstName);

	public User findByPrimaryContact(String primaryContact);

	public User findBySecurityQuestionAndSecurityAnswer(String securityQuestion, String securityAnswer);

	public List<User> findAllByKycStatus(String status);

	public Optional<User> findByEmailAndKycStatus(String emailId, String status);

	@Query("select u from User u where LOWER(u.email) = :tempEmailId")
	public User findByEmail2(String tempEmailId);

}
