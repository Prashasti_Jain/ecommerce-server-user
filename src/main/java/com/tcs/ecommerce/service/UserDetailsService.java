package com.tcs.ecommerce.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tcs.ecommerce.converter.AddressConverter;
import com.tcs.ecommerce.converter.UserConverter;
import com.tcs.ecommerce.entity.Address;
import com.tcs.ecommerce.entity.Role;
import com.tcs.ecommerce.entity.User;
import com.tcs.ecommerce.exception.EcommerceRestException;
import com.tcs.ecommerce.model.AddressModelRequest;
import com.tcs.ecommerce.model.AddressResponse;
import com.tcs.ecommerce.model.UserRegistrationRequest;
import com.tcs.ecommerce.model.UserRegistrationResponse;
import com.tcs.ecommerce.repository.AddressRepository;
import com.tcs.ecommerce.repository.RoleRepository;
import com.tcs.ecommerce.repository.UserRepository;

@Service
public class UserDetailsService {

	private UserDetailsService userDetailsService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public User findByemailId(String email) {
		return UserConverter.convertUserToResponse(userRepository.findByEmail(email));
	}

	public User fetchUserByUsername(@AuthenticationPrincipal String username) {
		return UserConverter.convertUserToResponse(userRepository.findByEmail(username));
	}

	public Address getAddress(@AuthenticationPrincipal String username) {
		List<Address> addr = addressRepository.findAllByUser(userRepository.findByEmail(username));
		if (addr.size() != 0) {
			return addr.get(0);
		}
		return new Address();
	}

	public List<Address> getAllMyAddress(@AuthenticationPrincipal String username) {
		List<Address> addr = addressRepository.findAllByUser(userRepository.findByEmail(username));
		List<Address> addrResp = new ArrayList<Address>();
		for (int i = 0; i < addr.size(); i++) {
			if (addr.get(i).getDeleteStatus() == null || !addr.get(i).getDeleteStatus().equalsIgnoreCase("deleted")) {
				addrResp.add(addr.get(i));
			}
		}
		if (addrResp.size() != 0 && addrResp != null) {
			return addrResp;
		}
		addrResp.add(new Address());
		return addrResp;
	}

	public List<Address> getMyAddresses(@AuthenticationPrincipal String username, int[] addrList) {
		List<Address> addrs = new ArrayList<Address>();
		for (int i = 0; i < addrList.length; i++) {

			Address address = addressRepository.findByAddressId(addrList[i]);
			if (address.getUser().getEmail().equalsIgnoreCase(username)) {
				addrs.add(address);
			}
		}
		return addrs;
	}

	public Address editMySingleAddress(Address addressReq, @AuthenticationPrincipal String username) {
		if (addressReq.getCurrent() == true) {
			this.changeDefaultAddress(username);
		}
		Address editAddr = addressRepository.findByAddressId(addressReq.getAddressId());
		editAddr.setAddress(addressReq.getAddress());
		editAddr.setCity(addressReq.getCity());
		editAddr.setContactNumber(addressReq.getContactNumber());
		editAddr.setState(addressReq.getState());
		editAddr.setZipCode(addressReq.getZipCode());
		editAddr.setFirstName(addressReq.getFirstName());
		editAddr.setLastName(addressReq.getLastName());
		editAddr.setModifiedBy(username);
		editAddr.setModifiedOn(new Date());
		editAddr.setCurrent(addressReq.getCurrent());
		addressRepository.save(editAddr);

		return editAddr;
	}

	public void changeDefaultAddress(String username) {
		List<Address> editAddrList = addressRepository.findAllByUser(userRepository.findByEmail(username));
		for (int i = 0; i < editAddrList.size(); i++) {
			editAddrList.get(i).setCurrent(false);
			addressRepository.save(editAddrList.get(i));
		}
	}

	public Address saveAddress(AddressModelRequest addressReq, @AuthenticationPrincipal String username) {
		if (addressReq.getCurrent() == true) {
			this.changeDefaultAddress(username);
		}
		Address address = AddressConverter.convertAddressModelToEntity(addressReq,
				userRepository.findByEmail(username));
		address.setDeleteStatus("Available");
		return addressRepository.save(address);
	}

	public String deleteMyAddressById(Integer addId, @AuthenticationPrincipal String username) {

		List<Address> addrList = addressRepository.findAllByUser(userRepository.findByEmail(username));
		for (int i = 0; i < addrList.size(); i++) {

			if (addrList.get(i).getAddressId().equals(addId)) {

				addrList.get(i).setDeleteStatus("deleted");
				addressRepository.save(addrList.get(i));
				return "{\"Value\":\"Deleted\"}";
			}
		}

		return "{\"Value\":\"Not Deleted\"}";
	}

	public UserRegistrationResponse registerUser(UserRegistrationRequest userRegistrationRequest) throws Exception {
		String tempEmailId = userRegistrationRequest.getEmail();
		String temPassword = userRegistrationRequest.getPassword();
		String encryptPWD = passwordEncoder.encode(temPassword);
		userRegistrationRequest.setPassword(encryptPWD);
		UserRegistrationResponse userRegistrationResponse = new UserRegistrationResponse();
		if (tempEmailId != null && !"".equals(tempEmailId)) {
			User userObj = userRepository.findByEmail2(tempEmailId.toLowerCase());

			if (userObj != null) {
				throw new EcommerceRestException("User with " + tempEmailId + " already exist", HttpStatus.BAD_REQUEST);
			}
		}
		Role role = roleRepository.findByRoleId(userRegistrationRequest.getRoleId());

		User savedEntity = userRepository
				.save(UserConverter.convertRegisterUserModeltoEntity(userRegistrationRequest, role));
		userRegistrationResponse.setEmail(savedEntity.getEmail());
		userRegistrationResponse.setFirstName(savedEntity.getFirstName());
		userRegistrationResponse.setLastName(savedEntity.getLastName());
		userRegistrationResponse.setPrimaryContact(savedEntity.getPrimaryContact());
		return userRegistrationResponse;
	}

	public static char[] generateOTP(int length) {

		String numbers = "0123456789";
		Random r = new Random();
		char[] otp = new char[length];
		for (int i = 0; i < length; i++) {
			otp[i] = numbers.charAt(r.nextInt(numbers.length()));
		}
		return otp;

	}

	public User sendOtp(String username) throws Exception {

		User tempUser = userRepository.findByEmail(username);

		if (tempUser == null) {
			throw new EcommerceRestException("User not found", HttpStatus.BAD_REQUEST);
		} else {

			char[] otp = userDetailsService.generateOTP(4);
			tempUser.setOtp(otp);
			userRepository.save(tempUser);
		}
		return UserConverter.convertUserToResponse(tempUser);
	}

	public User forgotPasswordByOTP(String password, char[] otp, String username) throws Exception {

		User tempUser = userRepository.findByEmail(username);

		if (tempUser == null) {
			throw new EcommerceRestException("User not found", HttpStatus.BAD_REQUEST);
		} else {

			if (Arrays.equals(otp, tempUser.getOtp())) {

				tempUser.setPassword(passwordEncoder.encode(password));
				userRepository.save(tempUser);

				return UserConverter.convertUserToResponse(tempUser);
			} else {
				throw new EcommerceRestException("OTP did not matched Please enter correct OTP",
						HttpStatus.BAD_REQUEST);

			}

		}

	}

	public boolean forgotpasswordByQuestion(User user) throws Exception {

		String tempQue = user.getSecurityQuestion();
		String tempAns = user.getSecurityAnswer();
		String tempPass = user.getPassword();
		User tempUser = userRepository.findByEmail(user.getEmail());
		if (tempUser == null) {
			throw new EcommerceRestException("User not found", HttpStatus.BAD_REQUEST);
		} else {
			if (tempQue.equals(tempUser.getSecurityQuestion()) && tempAns.equals(tempUser.getSecurityAnswer())) {
				tempUser.setPassword(passwordEncoder.encode(tempPass));
				userRepository.save(tempUser);

				return true;
			} else {
				throw new EcommerceRestException("Answer did not matched", HttpStatus.BAD_REQUEST);
			}
		}
	}

	public List<User> findInactiveUser(String kycStatus) {

		return userRepository.findAllByKycStatus(kycStatus);
	}

	public User approveUser(User user) {
		User userObj = userRepository.findByEmail(user.getEmail());

		if (userObj != null) {
			if (userObj.getKycStatus().equalsIgnoreCase("Pending")) {
				userObj.setKycStatus("Approved");
			} else if (user.getKycStatus().equalsIgnoreCase("Denied")) {
				userObj.setKycStatus("Denied");
			}
			userRepository.save(userObj);

		}
		return UserConverter.convertUserToResponse(userObj);
	}

	public User denyUser(User user) {
		User userObj = userRepository.findByEmail(user.getEmail());
		if (userObj != null) {
			if (user.getKycStatus().equalsIgnoreCase("Denied")) {
				userObj.setKycStatus("Denied");
			}
			userRepository.save(userObj);

		}
		return UserConverter.convertUserToResponse(userObj);
	}

	public User fetchUserByEmail(@AuthenticationPrincipal String username) {

		return UserConverter.convertUserToResponse(userRepository.findByEmail(username));
	}

	public User fetchUserByFirstName(String firstName) {
		return userRepository.findByFirstName(firstName);
	}

	public User fetchUserByConatctNumber(String primaryContact) {
		return userRepository.findByPrimaryContact(primaryContact);
	}

	public User updateUserDetails(UserRegistrationRequest user, @AuthenticationPrincipal String username)
			throws Exception {
		User tempUser = userRepository.findByEmail(username);
		if (tempUser == null) {
			throw new EcommerceRestException("User not found", HttpStatus.BAD_REQUEST);
		} else {

			tempUser.setFirstName(user.getFirstName());
			tempUser.setMiddleName(user.getMiddleName());
			tempUser.setLastName(user.getLastName());
			tempUser.setPrimaryContact(user.getPrimaryContact());
			tempUser.setAlternateContact(user.getAlternateContact());
			tempUser.setModifiedBy(username);
			tempUser.setModifiedOn(new Date());

		}

		userRepository.save(tempUser);
		return UserConverter.convertUserToResponse(tempUser);
	}

	public AddressResponse addAddress(AddressModelRequest address, @AuthenticationPrincipal String username)
			throws Exception {
		User tempUser = userRepository.findByEmail(username);
		AddressResponse savedResponse = new AddressResponse();
		if (tempUser == null) {
			throw new EcommerceRestException("User not found", HttpStatus.BAD_REQUEST);
		}
		Address addressEntity = addressRepository.save(AddressConverter.convertAddressModelToEntity(address, tempUser));
		savedResponse.setAddress(addressEntity.getAddress());
		savedResponse.setCity(addressEntity.getCity());
		savedResponse.setContactNumber(addressEntity.getContactNumber());
		savedResponse.setFirstName(addressEntity.getFirstName());
		savedResponse.setLastName(addressEntity.getLastName());
		return savedResponse;
	}

	public List<User> getUsers() {
		List<User> source = (List<User>) userRepository.findAll();
		List<User> pResp = new ArrayList<User>();
		for (int i = 0; i < source.size(); i++) {
			User temp = new User();
			temp.setUserId(source.get(i).getUserId());
			temp.setEmail(source.get(i).getEmail());
			temp.setRole(source.get(i).getRole());
			temp.setPrimaryContact(source.get(i).getPrimaryContact());
			temp.setKycStatus(source.get(i).getKycStatus());
			temp.setFirstName(source.get(i).getFirstName());
			temp.setMiddleName(source.get(i).getMiddleName());
			temp.setLastName(source.get(i).getLastName());
			temp.setGender(source.get(i).getGender());
			temp.setCreatedBy(source.get(i).getGender());
			temp.setCreatedOn(source.get(i).getCreatedOn());
			temp.setAlternateContact(source.get(i).getAlternateContact());

			pResp.add(temp);
		}
		return pResp;
	}

}
