package com.tcs.ecommerce.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tcs.ecommerce.entity.User;
import com.tcs.ecommerce.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(emailId);

		if (user == null) {
			throw new UsernameNotFoundException("user not found");
		}
		if (!user.getKycStatus().equalsIgnoreCase("Approved")) {
			throw new UsernameNotFoundException("KYC status not approved");
		}

		GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + user.getRole().getName());

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
				Arrays.asList(authority));
	}

}
